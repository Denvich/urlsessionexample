//
//  ViewController3.swift
//  URLSessionExample
//
//  Created by Denis Lyakhovich on 26.08.2021.
//

import UIKit

class ViewController3: UIViewController {
    
    @IBOutlet weak var titleIdView: UILabel!
    @IBOutlet weak var numberView: UILabel!
    @IBOutlet weak var imgaView: UIImageView!
    
    //обратный переход во VC2
    @IBAction func backNavigateToVc2(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //установка заголовка navigationcontroller
        navigationItem.title = "JSON Data"
        //вызов ApiServise отвечающих за загрузку и парсинг даныых из JSON
        loadData()
        //вызов ApiServise отвечающих за загрузку фото
        loadPhoto()
    }
}
