//
//  JsonApiServise.swift
//  URLSessionExample
//
//  Created by Denis Lyakhovich on 26.08.2021.
//

import Foundation
import UIKit

extension ViewController3 {

//MARK: - API для загрузки данных с сервера и парсинга данных из JSON  в объект data и отображения данных в пложении
func loadData() {
    //создаем константу с host аресом
    let stringURL = "http://denvich.com/api/data.json"
    //инициализируем константу URL, c проверкой на возврат через guard
    guard let url = URL(string: stringURL) else { return }
    //формируем задачу для получения данных их сети
    let task = URLSession.shared.dataTask(with: url) {
        //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) или ошибки(error)
        (data, response, error) in
        //обработка ошибок error
        if let error = error {
            print(error.localizedDescription)
            return
        }
        //обработка ошибок response
        if let response = response as? HTTPURLResponse {
            switch response.statusCode {
            case 200..<300:
                print("Status code: \(response.statusCode)")
                print("Response headers: \(response.allHeaderFields)")
                print("Serponse: \(response)")
                break
            default:
                print("Status code: \(response.statusCode)")
            }
            
        }
        //константа для работы с данными c проверкой на возврат через guard
        guard let data = data else { return }
        //парсинг данных - декодирование полученных даных из JSON в объкт data с проверкой guard на наличие данных, в случае отсутвия даннных в консоль выводится сообщение
        guard let jsonData = try? JSONDecoder().decode(DataNetwork.self, from: data) else {
            print("Error - Can't parse JSON DataNetwork")
            return
        }
        //обновление UI с учетом полученных и распаренных данных через GCD в главном потоке
        DispatchQueue.main.async {
            //обновление outlet titleIdView полученными данными titleId
            self.titleIdView.text = jsonData.titleId
            //обновление outlet numberView полученными данными number
            self.numberView.text = jsonData.number
            //вывод в консоль поученных данных
            print("DataNetwork: \(jsonData.titleId)")
            print("DataNetwork: \(jsonData.number)")
        }
       
    }
    //вызов задачи
    task.resume()
}

//MARK: - API для загрузки фото с сервера и отображения их в пложении
func loadPhoto() {
    //создаем константу с host аресом
    let stringURL = "http://denvich.com/api/img/gallery/flower/pic9.png"
    //инициализируем константу URL, c проверкой на возврат через guard
    guard let url = URL(string: stringURL) else { return }
    //формируем задачу для получения данных их сети
    let task = URLSession.shared.dataTask(with: url) {
        //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) или ошибки(error)
        (data, response, error) in
        //обработка ошибок error
        if let error = error {
            print(error.localizedDescription)
            return
        }
        //обработка ошибок response
        if let response = response as? HTTPURLResponse {
            switch response.statusCode {
            case 200..<300:
                print("Status code: \(response.statusCode)")
                print("Response headers: \(response.allHeaderFields)")
                print("Serponse: \(response)")
                break
            default:
                print("Status code: \(response.statusCode)")
            }
        }
        //констатда для работы  с полученными данными
        guard let data = data else { return }
        DispatchQueue.main.async {
            //приведение типа data полученного с сервера к типу картинки UIImage
            let image = UIImage(data: data)
            //обновление outlet imageView полученными данными
            self.imgaView.image = image
        }
    }
    //вызов задачи
    task.resume()
}    
}
