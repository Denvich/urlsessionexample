//
//  JsonData.swift
//  URLSessionExample
//
//  Created by Denis Lyakhovich on 26.08.2021.
//

import Foundation

//Струкутра для декодирования в объект данных получаемых с сервера в формате JSON
struct DataNetwork: Decodable {
    let titleId: String
    let number: String
}


