//
//  ViewController2.swift
//  URLSessionExample
//
//  Created by Denis Lyakhovich on 25.08.2021.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    //обратный переход в VC
    @IBAction func backNavigateToVc(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //установка заголовка navigationcontrller
        navigationItem.title = "Red Roses"
        //создаем константу с host аресом
        let string = "http://denvich.com/api/img/gallery/flower/pic3.png?"
        //инициализируем константу URL, c проверкой на возврат через guard
        guard let url = NSURL(string: string) else {return}
        //формируем задачу для получения данных их сети
        let task = URLSession.shared.dataTask(with: url as URL) {
            //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) или ошибки(error)
            (data, response, error) in
            print("Load Done")
            //обработка ошибок коплишн блока
            //error
                if let error = error {
                    print(error.localizedDescription)
                    return
                }
            //response
                if let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200..<300:
                        print("Status code: \(response.statusCode)")
                        break
                    default:
                        print("Status code: \(response.statusCode)")
                    }
                }
            //константа для работы с данными c проверкой на возврат через guard
            guard let data = data else {return}
            //обновление UI с учетом полученных данных от сервера через GCD в главном потоке
            DispatchQueue.main.async {
                //приведение типа data полученного с сервера к типу картинки UIImage
                let image = UIImage(data: data)
                //обновление outlet imageView полученными данными
                self.imageView.image = image
            }
        }
        //вызов задачи
        task.resume()
    }
}
