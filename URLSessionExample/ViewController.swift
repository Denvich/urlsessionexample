//
//  ViewController.swift
//  URLSessionExample
//
//  Created by Denis Lyakhovich on 25.08.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //установка заголовка навигации через свойство navigationItem
        navigationItem.title = "Violet Roses"
        //создаем константу с host аресом
        let string = "http://denvich.com/api/img/gallery/flower/pic2.png?"
        //инициализируем константу URL, c проверкой на возврат через guard
        guard let url = URL(string: string) else {return}
        //формируем задачу типа dataTask для получения данных из сети через URLSession
        let task = URLSession.shared.dataTask(with: url as URL) {
            //комплишн замыкание возвраающее ответ(response) от сервера в виде данных(data) или ошибки(error)
            (data, response, error) in
            print("Load Done")
            //константа для работы с данными c проверкой на возврат через guard
            guard let data = data else {return}
            //обновление UI с учетом полученных данных от сервера через GCD в главном потоке
            DispatchQueue.main.async {
                //приведение типа data полученного с сервера к типу картинки UIImage
                let image = UIImage(data: data)
                //обновление outlet imageView полученными данными
                self.imageView.image = image
            }
        }
        //вызов задачи
        task.resume()
    }
    
    @IBAction func unwindSegue(for unwindSegue: UIStoryboardSegue) {
        print("Exit")
    }
    
    //ОБЯЗАТЕЛЬНО в файле Info.plist
    //добавляем - App Transport Security Settings - Allow Arbitrary Loads - YES,
    //в ином случае данные не бубут загружаться с указанного URL
}

